import React from "react";
import Order from "../Order/Order";
import Header from "../Header/Header";
import Inventory from "../Inventory/Inventory";

import "./App.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      books: [],
      order: []
    };
  }

  addNewBook = book => {
    let newBooks = [...this.state.books];

    newBooks.push(book);

    this.setState({
      books: newBooks
    });
  };

  addToOrder = book => {
    this.setState(prevState => {
      if (prevState.order.indexOf(book) === -1) {
        return { order: [...prevState.order, book] };
      }
    });
  };

  removeFromOrder = title => {
    this.setState({
      order: this.state.order.filter(book => title !== book.name)
    });
  };

  render() {
    return (
      <div className="app container">
        <Header />
        <div className="row">
          <Order
            order={this.state.order}
            removeFromOrder={this.removeFromOrder}
          />
          <Inventory books={this.state.books} addToOrder={this.addToOrder} />
        </div>
      </div>
    );
  }
}

export default App;
