import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { shallow, configure } from "enzyme";
import ReactSixteenAdapter from "enzyme-adapter-react-16";
import { BrowserRouter } from "react-router-dom";

configure({ adapter: new ReactSixteenAdapter() });

describe("App tests", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <BrowserRouter>
        <App />
      </BrowserRouter>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });
  it("Child components render", () => {
    const wrapper = shallow(<App />);
    // console.log(wrapper.debug());
    expect(wrapper.find("Header").exists()).toBe(true);
    expect(wrapper.find("Order").exists()).toBe(true);
    // expect(wrapper.find("AdminPanel").exists()).toBe(true);
    expect(wrapper.find("Inventory").exists()).toBe(true);
  });
  it("Snapshot matches", () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
});
