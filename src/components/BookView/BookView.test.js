import React from "react";
import BookView from "./BookView";

// import * as Enzyme from "enzyme";
import { shallow, configure } from "enzyme";
import ReactSixteenAdapter from "enzyme-adapter-react-16";

configure({ adapter: new ReactSixteenAdapter() });
// configure({ adapter: new Adapter() });

describe("Order view tests", () => {
  it("Snapshot matches", () => {
    const book = {
      name: "Book title",
      author: "Test",
      image: "/img/img.png"
    };
    const wrapper = shallow(<BookView book={book} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("Title of the book is displayed as in props", () => {
    const book = {
      name: "Book title",
      author: "Test",
      image: "/img/img.png"
    };
    const wrapper = shallow(<BookView book={book} />);
    expect(
      wrapper
        .find(".bookView")
        .find("b")
        .text()
    ).toBe(book.name);
  });
});
