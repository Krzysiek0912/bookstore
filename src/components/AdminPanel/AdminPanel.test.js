import React from "react";
import ReactDOM from "react-dom";
import AdminPanel from "./AdminPanel";

// import * as Enzyme from "enzyme";
import { shallow, configure } from "enzyme";
import ReactSixteenAdapter from "enzyme-adapter-react-16";

configure({ adapter: new ReactSixteenAdapter() });
// configure({ adapter: new Adapter() });

describe("AdminPanel tests", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<AdminPanel />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it("Snapshot matches", () => {
    const wrapper = shallow(<AdminPanel />);
    expect(wrapper).toMatchSnapshot();
  });
});
