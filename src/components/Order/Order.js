import React, { Component } from "react";
import "./Order.css";
import OrderView from "../OrderView/OrderView";

class Order extends Component {
  render() {
    const ordersView = this.props.order.map(order => {
      return (
        <OrderView
          key={order.name}
          book={order}
          removeFromOrder={this.props.removeFromOrder}
        />
      );
    });

    return (
      <div className="order col-xs-6">
        <h2>Your order:</h2>
        {ordersView}
      </div>
    );
  }
}

export default Order;
