import React from "react";
import ReactDOM from "react-dom";
import Inventory from "./Inventory";

// import * as Enzyme from "enzyme";
import { shallow, configure } from "enzyme";
import ReactSixteenAdapter from "enzyme-adapter-react-16";

configure({ adapter: new ReactSixteenAdapter() });
// configure({ adapter: new Adapter() });

describe("Inventory tests", () => {
  it("Inventory renders without a problem", () => {
    const div = document.createElement("div");
    const books = [];
    ReactDOM.render(<Inventory books={books} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("Snapshot matches", () => {
    const books = [];
    const wrapper = shallow(<Inventory books={books} />);
    expect(wrapper).toMatchSnapshot();
  });
});
