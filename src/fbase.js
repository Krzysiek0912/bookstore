import Rebase from "re-base";
import firebase from "firebase";

const config = {
  apiKey: "AIzaSyCfQM9CJCNDtOBAxqDpmZZSJzYYNRnIEJU",
  authDomain: "bookstore2-eb04f.firebaseapp.com",
  databaseURL: "https://bookstore2-eb04f.firebaseio.com",
  projectId: "bookstore2-eb04f",
  storageBucket: "",
  messagingSenderId: "772313764931"
};
const firebaseApp = firebase.initializeApp(config);
const fbase = Rebase.createClass(firebaseApp.database());

export { fbase, firebaseApp };
